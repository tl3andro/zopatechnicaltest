#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys, csv, os.path

# Market Data for Technical exercise:
# Lender,Rate,Available
data = [
    ('Bob', 0.075, 640),
    ('Jane', 0.069, 480),
    ('Fred', 0.071, 520),
    ('Mary', 0.104, 170),
    ('John', 0.081, 320),
    ('Dave', 0.074, 140),
    ('Angela', 0.071, 60)
]


class Quote:
    def __init__(self, lenders, requested_amount, n=36):
        self.n = n
        self.lenders = lenders
        self.requested_amount = requested_amount

    def sort_lenders(self):
        """
        Order leners by rate
        """
        self.lenders = sorted(self.lenders, key=lambda x: x[1])

    def has_offers(self):
        """
        Checks if the markets has enough offers
        :rtype: boolean
        """
        return sum([lender[2] for lender in self.lenders]) >= self.requested_amount

    def is_valid_requested_amount(self):
        """
        Checks if the borrowed amount is valid
        :rtype: boolean
        """
        return (100 <= self.requested_amount <= 15000) and self.requested_amount % 100 == 0

    @property
    def get_quote(self):
        """
        Calculates a quote based on a market lenders and the requested loan amount
        :rtype:dict
        """
        self.sort_lenders()
        lenders = list(self.lenders)

        total_lended_amount = 0
        quote = {'month_payment': 0.,
                 'total_repay': 0.,
                 'rate': 0.}

        while total_lended_amount < self.requested_amount:
            lender = lenders.pop(0)
            lender_amount = min(self.requested_amount - total_lended_amount, lender[2])
            total_lended_amount += lender_amount

            # Calculate partial quote
            assert isinstance(lender, (tuple,))
            quote['month_payment'] += -np.pmt(lender[1] / 12, self.n, lender_amount)
            quote['rate'] += lender_amount * lender[1]

        # Monthly repayment
        quote['month_payment'] = round(quote['month_payment'], 2)
        # Weighted average
        quote['rate'] = round((quote['rate'] / self.requested_amount) * 100, 1)
        # Total repayment
        quote['total_repay'] = round(quote['month_payment'] * self.n, 2)

        return quote


def run():
    def read_input():
        """
        Read input values passed as argument
        arg1: csv file path with the list of lenders
        arg2: loan_amount
        """
        assert len(sys.argv) == 3 and sys.argv[2].isdigit()
        loan_amount = int(sys.argv[2])

        #Read CSV file
        assert os.path.exists(sys.argv[1]), "File %s does not exist." % sys.argv[1]
        with open(sys.argv[1], 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            market = []
            for row in reader:
                if row:
                    market += [(row[0], float(row[1]), float(row[2]))]
        return (market, loan_amount)

    def print_output(loan_amount, quote):
        """
        Given an amount prints the quote parameters
        :param loan_amount:
        :param quote: dictionary with the code values
        """
        assert quote and isinstance(quote, dict)
        output = """
        Requested amount: £%s
        Rate: %.1f%%
        Monthly repayment: £%.2f
        Total repayment: £%.2f
        """ % (loan_amount, quote['rate'], quote['month_payment'], quote['total_repay'])
        print output

    # Read the parameters and create an instance of the quote
    market, loan_amount = read_input()
    q = Quote(market, loan_amount)

    # Is a accepted amount?
    if not q.is_valid_requested_amount():
        print  "Loan amount must be between £1000 and £15000 with a increment of any £100."
        return

    # The market has enough offers?
    if not q.has_offers():
        print "It is not possible to provide a quote."
        return

    # Calculate the quote
    quote = q.get_quote
    print_output(loan_amount, quote)


if __name__ == '__main__':
    run()
