#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from quote import Quote

market_1 = [
    ('Bob', 0.075, 640),
    ('Jane', 0.069, 480),
    ('Fred', 0.071, 520),
    ('Mary', 0.104, 170),
    ('John', 0.081, 320),
    ('Dave', 0.074, 140),
    ('Angela', 0.071, 60)
]


class QuoteTestCase(unittest.TestCase):
    def test_has_offers(self):
        q = Quote(market_1, 2300)
        assert q.has_offers() == True
        q = Quote(market_1, 2400)
        assert q.has_offers() == False

    def test_get_quote(self):
        q = Quote(market_1, 1000)
        quote = q.get_quote
        assert quote['month_payment'] == 30.88
        assert quote['rate'] == 7.0
        assert quote['total_repay'] == 1111.68

    def test_is_valid_requested_amount(self):
        q = Quote(market_1, 100)
        assert q.is_valid_requested_amount() == True
        q = Quote(market_1, 1001)
        assert q.is_valid_requested_amount() == False
        q = Quote(market_1, 50)
        assert q.is_valid_requested_amount() == False
        q = Quote(market_1, 15500)
        assert q.is_valid_requested_amount() == False

if __name__ == '__main__':
    unittest.main()
